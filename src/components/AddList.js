import React, {Component} from 'react'
import axios from 'axios'
import SaveIcon from "@material-ui/icons/Save"
import CancelIcon from "@material-ui/icons/Cancel"
import {Grid, TextField, Button} from '@material-ui/core'


class AddList extends Component{
    constructor(props){
        super(props)

        this.state = {
            Course: {
                id: "",
                title: "",
                category: "",
                authorId: "",
                watchHref: ""
            }
        }

        this.saveCourse = this.saveCourse.bind(this)
        this.onChangeCourse = this.onChangeCourse.bind(this)
    }

   


    async componentDidMount(){
        let id = this.props.match.params.id
        
        if(!id)
            return
        let element = await axios.get('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses/'+id)
        
        if(element.hasErrors){
            alert.apply(element.message)
            return 
        }
        this.setState({Course: element.data})
        
    }

    onChangeCourse(e){
        let newCourse = this.state.Course;
        newCourse[e.target.name] = e.target.value
        this.setState({Course: newCourse})
    }

    async saveCourse(){
        let id = this.props.match.params.id
        if(!id){
            console.log(this.state.Course)
            let res = await axios.post('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses',this.state.Course)
            if(res.hasErrors){
                alert(res.message)
                return 
            }
            this.props.history.push("/")
        }else{
            let res = await axios.put('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses/'+ id,this.state.Course)
            console.log(this.state.Course)
            if(res.hasErrors){
                alert(res.message)
                return 
            }
            this.props.history.push("/")  
        }
    }

    render(){
        return(
            <div>
                <Grid container spacing={3}>
                    <Grid item xs={4} sm={4}>
                        <TextField 
                            autoFocus
                            margin="dense"
                            id="id"
                            name="id"
                            label= "CourseID"
                            fullWidth
                            value = {this.state.Course.id}
                            onChange={e => this.onChangeCourse(e)}
                            >
                        </TextField> 
                    </Grid>
                    <Grid item xs={4} sm={4}>
                        <TextField 
                            autoFocus
                            margin="dense"
                            id="Title"
                            name="title"
                            label= "Title"
                            fullWidth
                            value = {this.state.Course.title}
                            onChange={e => this.onChangeCourse(e)}
                            >
                        </TextField> 
                    </Grid>
                    <Grid item xs={4} sm={3}>
                        <TextField 
                            autoFocus
                            margin="dense"
                            id="Category"
                            name="category"
                            label= "Category"
                            fullWidth
                            value = {this.state.Course.category}
                            onChange={e => this.onChangeCourse(e)}
                            >
                        </TextField> 
                    </Grid>
                    <Grid item xs={6} sm={3}>
                        <TextField 
                            autoFocus
                            margin="dense"
                            id="Author"
                            name="authorId"
                            label= "Author"
                            fullWidth
                            value = {this.state.Course.authorId}
                            onChange={e => this.onChangeCourse(e)}
                            >
                        </TextField> 
                    </Grid>
                </Grid>
                <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', marginTop:'25px'}}>
                <Button style={{ backgroundColor: '#0f49ba', color: 'white', marginRight: '10px', borderRadius: '50px'}} variant="outlined" startIcon={<CancelIcon/>} onClick={
                    (()=> {this.props.history.push("/")})
                }>
                    Cancel
                </Button>
                <Button onClick={this.saveCourse} style={{ backgroundColor: '#0f49ba' ,color: 'white', borderRadius: '50px'}} variant="outlined" startIcon={<SaveIcon/>}>
                    Save
                </Button>
                </div>
                
            </div>
        )
    }
}


export default AddList;