import React, {Component} from 'react';
import 'axios'
import axios from 'axios';
import AddIcon from '@material-ui/icons/Add';
import {Button, Paper,Table, TableBody, TableCell, TableRow, TableContainer, TableHead, IconButton, Tooltip} from '@material-ui/core';
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone';
import EditTwoTone from '@material-ui/icons/EditTwoTone';
import Box from '@material-ui/core/Box';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class TableList extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            rows: []
        }

        this.deleteCourse = this.deleteCourse.bind(this);
    }

    async componentDidMount(){
        let data = await axios.get('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses');
        
        
        if(data.hasErrors){
            alert(data.message);
            return;
        
        }    
        
        this.setState({rows: data.data})
      
        
        
    }

    async deleteCourse(id, index){
        let res = await axios.delete('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses/' + id)
        if(res.hasErrors){
            alert(res.message);
            return;
        }

        let courses = this.state.rows;
        courses.splice(index, 1);
        this.setState({rows: courses});
    }

    render(){
        return(
           
            <div>
                <div style={{display: 'flex', flexDirection: 'column', justifyContent:'center', color: '#ffffff', alignItems:'center', background:"#0f49ba"}}> 
                <br/>
                <br/>
                <h1>What course do you want to teach today?</h1>
                    <Button id="addCourse"
                        style={{borderRadius: '50px', backgroundColor: '#ffffff',color: '#0f49ba', fontWeight: 'bold', padding: '10px', marginBottom: '20px'}}
                        variant="contained"
                        color="primary"
                        startIcon={<AddIcon />}
                        onClick={()=>{
                            this.props.history.push("/AddCourse")
                        }}
                    >
                        Add new Course
                    </Button>
                </div>
                <br/>
                <Box m={3}>
                <TableContainer component={Paper} style={{borderRadius: '25px'}}>
                    <Table aria-label="simple table">
                        <TableHead style={{backgroundColor: '#0f49ba'}}>
                            <TableRow>
                                <TableCell style={{color: 'white'}} aling="right">Title</TableCell>
                                <TableCell style={{color: 'white'}} aling="right">Category</TableCell>
                                <TableCell style={{color: 'white'}} aling="right">Author</TableCell>
                                <TableCell style={{color: 'white'}} aling="right">Link</TableCell>
                                <TableCell style={{color: 'white'}} aling="right">Edit/Delete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.rows.map((row,index)=>(
                                <TableRow key={row.id}>
                                    <TableCell  component="th" scope="row">{row.title}</TableCell>
                                    <TableCell  aling="right">{row.category}</TableCell>
                                    <TableCell  aling="right">{row.authorId}</TableCell>
                                    <TableCell  aling="right">{row.watchHref} <FileCopyIcon style={{marginLeft: '10px', height: '20px', color: '#0f49ba' }} onClick={() => 
                                        {
                                            navigator.clipboard.writeText(row.watchHref)
                                            alert('Link has been copied to clipboard')
                                        }}></FileCopyIcon> </TableCell>
                                    <TableCell  aling="right">
                                        <IconButton onClick = {(()=>{this.props.history.push(`/AddCourse/${row.id}`)})}>
                                            <EditTwoTone style={{ color: '#0f49ba' }} />
                                        </IconButton>
                                        <IconButton onClick ={()=> this.deleteCourse(row.id,index)}>
                                            <DeleteTwoToneIcon color="secondary" />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                </Box>
            </div>
        )
    }
}

export default TableList;
