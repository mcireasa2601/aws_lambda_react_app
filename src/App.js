
import './App.css';
import React, {Component} from 'react';
import TableList from './components/TableList';
import AddList from './components/AddList';
import {BrowserRouter, Route,Switch} from 'react-router-dom';

class App extends Component{
  render(){
    return (
      <BrowserRouter>
      <Switch>
        <Route path = '/' exact strict component = {TableList}/>
        <Route path = '/AddCourse/:id?' exact strict component = {AddList}/>
      </Switch>
      </BrowserRouter>
    );
  }
 
}

export default App;
