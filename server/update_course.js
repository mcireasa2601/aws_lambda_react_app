const AWS = require('aws-sdk');
const axios = require('axios');
const dynamo = new AWS.DynamoDB.DocumentClient();

const { TABLE_NAME: TableName } = process.env;
const { YOUTUBE_KEY: YoutubeKey } = process.env;

exports.handler = async (event, context) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Headers" : "Content-Type",
    "Access-Control-Allow-Origin": '*',
    'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  };
  
  try{
    var data = JSON.parse(event.body)
    
     //Get youtube url based on coruse's title
    
    let url = 'https://youtube.googleapis.com/youtube/v3/search?q=' + data.title + '&key=' +YoutubeKey+ '&type=video&part=snippet'
    let response = await axios.get(url)
    let first_url = "https://www.youtube.com/watch?v=" + response.data.items[0].id.videoId
    
    
    
    await dynamo.update({
      TableName: TableName,
      Key:{
        id: event.pathParameters.id
      },
      UpdateExpression: "set title = :title, category = :category, authorId = :authorId, watchHref = :watchHref",
      ExpressionAttributeValues:{
        ':title': data.title,
        ':authorId': data.authorId,
        ':category': data.category,
        ':watchHref': first_url
      },
      ReturnValues:"UPDATED_NEW"
    }).promise()
    
  }catch (err) {
    statusCode = 400;
    body = err.message;
  }finally{
    body = JSON.stringify(body);
  }
  return {
    statusCode,
    body,
    headers
  };
}