const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

const { TABLE_NAME: TableName } = process.env;

exports.handler = async (event, context) => {
  let body;
  let statusCode = 200;
   const headers = {
        "Access-Control-Allow-Headers" : "Content-Type",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET,DELETE,UPDATE"
  };
  try{
    body = await dynamo.delete({
      TableName: TableName,
      Key: {
        id: event.pathParameters.id
      }
    }).promise()
  }catch (err) {
    statusCode = 400;
    body = err.message;
  }finally{
    body = JSON.stringify(body);
  }
  return {
    statusCode,
    body,
    headers
  };
}