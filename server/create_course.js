const AWS = require('aws-sdk');
const axios = require('axios')
const dynamo = new AWS.DynamoDB.DocumentClient();

const { TABLE_NAME: TableName } = process.env;

const {YOUTUBE_API_KEY: Youtube_Api_Key } = process.env;

exports.handler = async (event, context) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Headers" : "Content-Type",
    "Access-Control-Allow-Origin": '*',
    'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  };
  try{
    //Get youtube url based on course title
    let data = JSON.parse(event.body)
    let url = 'https://youtube.googleapis.com/youtube/v3/search?q=' + data.title + '&key='+ Youtube_Api_Key +'&type=video&part=snippet'
    let response = await axios.get(url)
    let first_url = "https://www.youtube.com/watch?v=" + response.data.items[0].id.videoId
    
    //Create object
    var params ={
      TableName: TableName,
      Item:{
      id:data.id,
      title: data.title,
      authorId: data.authorId,
      category: data.category,
      watchHref: first_url
  }
    }
    
    await dynamo.put(params).promise()
    
  }catch (err) {
    statusCode = 400;
    body = err.message;
  }finally{
    body = JSON.stringify(body);
  }
  return {
    statusCode,
    body,
    headers
  };
}



