const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

const { TABLE_NAME: TableName } = process.env;

exports.handler = async (event, context, callback) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Headers" : "Content-Type",
    "Access-Control-Allow-Origin": '*',
    'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  };
  try{
    var params = {
      TableName: TableName,
      Select: "ALL_ATTRIBUTES"
    };
    let scanResults = [];
    let items;
    do{
        items =  await dynamo.scan(params).promise();
        items.Items.forEach((item) => scanResults.push(item));
        params.ExclusiveStartKey  = items.LastEvaluatedKey;
    }while(typeof items.LastEvaluatedKey !== "undefined");
    
    body = scanResults
  
  }catch (err) {
    statusCode = 400;
    body = err.message;
  }finally{
    body = JSON.stringify(body);
  }
  return {
    statusCode,
    body,
    headers
  };
}