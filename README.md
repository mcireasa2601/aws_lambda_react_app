# Courses Catalog 

Check the application at the link below:

http://courses-mcr.s3-website.us-east-2.amazonaws.com/ 

**Introduction**:

Because of the current events that happened because of the Covid Pandemic, more and more people have found themselves in a new situation: a situation where they would like to learn new skills but are unable to physically attend courses. This has naturally changed the way courses are taught, in the sense that almost everything has moved online. So, if you are in search of a new skill, chances are we can find it online.

**Problem that we aim to solve:**

With the rapid growth of online courses, has also come an increased in online teachers. Because teachers can easily get caught up into planning new courses and topics to teach, I have created an application that helps them keep a tracker of courses that they would want to implement. Furthermore, the application also gives them suggestions from YouTube content creators that have also touched on the topics that they want to teach. 

**APIs Description:**

In order to create the logic behind the application I have used two APIs. 

_**First one:**_
An own created API that integrates with a couple of AWS Services in order to provide the logic behind the app. The API sits behind AWS API Gateway which allows us to create RESTful APIs. Once a call is made to one of the Endpoints created in AWS API Gateway, an AWS Lambda Function is triggered based on this event. The Lambda Function’s logic is written using NodeJS. After the Lambda functions has run, the output of it will be saved in AWS DynamoDB.

_**Second one:**_
The second RESTful API that I am using is the YouTube API. With the YouTube API you can upload videos, manage playlists and subscriptions, update channel settings, and more. We can also use the API to search for videos matching specific search terms, topics, locations, publication dates, and much more. The APIs search.list method also supports searches for playlists and channels. In this particular example I have chosen to use the search.list method based on a keyword.

**Data flow:**

To give an example of the data flow I have chosen the context when a user wants to create a new course. The flow will be the following: user access the webpage by making a request to the website’s URL from its browser. Amazon S3 will serve the static page to the user and the browser will load the page. The user will then fill in a form to create a new course. After the form has been filled, the details will be sent to AWS API Gateway. Here, all the information received from the client will be forwarded to one of the Lambda functions which is tasked with saving the new course in DynamoDB Database. Before sending the information to be saved, Lambda will make a call to the YouTube API to retrieve a suggested video based on the course’s title. After it gets a positive response from the YouTube API alongside a suggested video information, the Function will save the data into DynamoDB.

Here is a diagram of the above flow: 

![](images/Serverless_Data_Flow.png)

_**And here are also the HTTP Methods used:**_

List all course (GET): 

``` 
let data = await axios.get('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses')
```
{: .language-javascript}

Create course (POST): 

```
let res = await axios.post('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses',this.state.Course)
```
{: .language-javascript}

Update a course (PUT):

```
let res = await axios.put('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses/'+ id,this.state.Course)
```
{: .language-javascript}
  
Delete a course (DELETE):

```
let res = await axios.delete('https://lhdbkwryl7.execute-api.us-east-2.amazonaws.com/courses/' + id)
```
{: .language-javascript}

Last thing to mention, in order to use the YouTube API, we will need to create an API key for our Google Developer Account. The key will be sent with each request to the API in order to authenticate to it and be able to use it. I will link the documentation that needs to be followed here: 
https://developers.google.com/youtube/v3/guides/authentication 


**Application’s Screenshots:**

![](images/Landing_Page.png)

![](images/Create_Course.png)

![](images/Edit_Course.png)
